import numpy as np
import scipy.fftpack as ft
import matplotlib.pyplot as plt
from pylab import random, gray, imread, imsave
import os

def slm_array(size, px, ff): #size is the "pixel" of slm, px is size of one "pixel" used to apply fill factor
    slm_size = size*px       #effective size of slm in pixels
    mn = sum(np.linspace(0,px-1,px))/px
    arr = np.zeros([slm_size,slm_size])
    arr_xy = abs(np.linspace(0,slm_size-1, slm_size)%px - mn) - np.sqrt(ff)*mn
    arr_b = (arr_xy<=0)*1
    arr2 = arr.T
    for i in range(slm_size):
        arr[i,:] = arr_b
    arr2 = arr.T
    return arr * arr2

def circle(size, af):
    X = np.zeros([size,size])
    X[:,] = abs(np.linspace(0,size-1,size)-size/2)
    return (np.sqrt(X**2 + (X.T)**2) < af*size/2.)*1.

##master's code
def phase(x):
        p = np.angle(x) #there is a built-in function in numpy to compute for the angle
        return p
        
def amp(y):
        a = np.abs(y)
        return a

def normal(z): #not required for GS algorithm. This is used only for saving images
        I=(z-np.min(np.min(z)))/(np.max(np.max(z))-np.min(np.min(z)))
        return I

def gerchberg_saxton(S,T):
        #note: look at the places where i used fftshift.  Remember your lens performs an optical FT which does NOT
        #require an fftshift while the function fft2 does.  When writing your code, imagine the physical setup.
        dim=np.shape(S)
        guess = random(dim)*2*np.pi
        a = ft.ifft2(amp(ft.fftshift(T))*np.exp(1j*random(dim)*2*np.pi))  #py.random is not really required. this just serves as a starting "guess" for the phase
        for m in range(1437):
            b = amp(S)*np.exp(1j*phase(a))
            c = ft.fft2(b)
            d = amp(ft.fftshift(T))*np.exp(1j*phase(c))
            a = amp(S)*np.exp(1j*phase(ft.ifft2(d)))
        Rphase=phase(a)
        return Rphase


def rescale_to_slm(px, holo):
    #s is the source with fill factor, obviously the size is greater than the holo
    #holo is hologram from gs. size is small (emulating slm pixel).
    w = len(holo)
    pix = px*w
    s = np.zeros([pix,pix], dtype = complex)   
    for i in range(w):
        for l in range(w):
            s[i*px:(i+1)*px,l*px:(l+1)*px] = holo[i,l]
    return s
def f(expt, true_sig): #fidelity
    ar3 = (np.sum((amp(true_sig)-amp(expt))**2))/(np.sum(amp(true_sig)**2))
    return 1-ar3

def power_norm(ar1, ar2):
    ar3 = amp(ar1)/np.sqrt(np.sum(amp(ar1)**2))
    return ar3*np.sqrt(np.sum(amp(ar2)**2))

def corr(expt, true_sig): #correlation_quality
    return np.sum(amp(expt)*amp(true_sig))/np.sum(amp(true_sig)**2)
    
def struct(expt, true_sig): #structural content
    return np.sum(amp(expt)**2)/np.sum(amp(true_sig)**2)

def sig2noise(expt, ideal):
    a= expt*1.0*(ideal>0)
    b = np.sum(a*np.conj(a))
    c = np.sum(expt*np.conj(expt))
    return (b/(c-b))

#setting the parameters
n=256
j = 1000
slm_p = 192                    #number of slm pixels
px = 10                        #division of pixels, to be used to simulate fill factor
ff = 0.64                               #fill factor of slm
af = 0

target = imread("target.png")
s = circle(px*slm_p, 1)         #Source for expanded
s1 = s*slm_array(slm_p, px, ff) #source with dead spaces
s5 = abs(s-s1)                  #deadpixels
s6 = circle(slm_p, 1)           #Source for not expanded
t = np.linspace(0, 3*np.pi, n)

zod_big = ft.fftshift(ft.fft2(s5))
zod = zod_big[960-96:960+96,960-96:960+96]
zod_phase = phase(zod_big[960,960])
for l in range(j):
    k = l + 461
    path1 = r'C:\Documents and Settings\atchong\Desktop\wynn\current_work\1st_sem_2014_2015\Research\aug_26_2014\run' + str(k) + r'\reconstruction'
    path2 = r'C:\Documents and Settings\atchong\Desktop\wynn\current_work\1st_sem_2014_2015\Research\aug_26_2014\run' + str(k) + r'\zod_hologram'
    path3 = r'C:\Documents and Settings\atchong\Desktop\wynn\current_work\1st_sem_2014_2015\Research\aug_26_2014\run' + str(k) + r'\zod_target_hologram'
    path = r'C:\Documents and Settings\atchong\Desktop\wynn\current_work\1st_sem_2014_2015\Research\aug_26_2014\run' + str(k)
    if not os.path.exists(path1):
        os.makedirs(path1)
    if not os.path.exists(path2):
        os.makedirs(path2)
    if not os.path.exists(path3):
        os.makedirs(path3)
    if not os.path.exists(path):
        os.makedirs(path)

    rel_int = np.zeros(n)
    rel_phase = np.zeros(n)
    zod_hologram = gerchberg_saxton(amp(s6), (amp(zod))) * s6
    
    target_field = amp(s6)*np.exp(1j*gerchberg_saxton(amp(s6), amp(target)) * s6)
    target_field_exp = rescale_to_slm(px, target_field)*s1 + s5
    target_recon = ft.fftshift(ft.fft2(target_field_exp))
    target_recon_int = target_recon*np.conj(target_recon)
    zod_nosupp = np.sum(target_recon_int[960-13:960+13,960-13:960+13])
    m = np.max(abs(target_recon*np.conj(target_recon)))
    
    for i in range(n):
        phase_shift = t[i]
        zod_holo = zod_hologram + phase_shift
        field_all = amp(s6)*np.exp(1j*(zod_holo)) +target_field
        metho_field = amp(s6)*np.exp(1j*phase(field_all))
        new_holo_field_expand = rescale_to_slm(px, metho_field)*s1 + s5
        new_recon = ft.fftshift(ft.fft2(new_holo_field_expand))
        new_recon_int = new_recon*np.conj(new_recon)
        zod_new = np.sum(new_recon_int[960-13:960+13,960-13:960+13])
        filename = 'reconzod'+str(k) + 'run' + str(i) +'.png'
        filename2 = 'zod_hologram'+ str(k) + 'run' + str(i) + '.png'
        filename3 = 'zod_target_hologram'+ str(k) + 'run' + str(i) + '.png'
        f1 = os.path.join(path1, filename)
        f2 = os.path.join(path2,filename2)
        f3 = os.path.join(path3,filename3)
        imsave(f1,abs(new_recon_int[960-13:960+13,960-13:960+13]),vmin = 0, vmax = m,  cmap = gray())
        imsave(f2, abs((zod_holo + 2*np.pi)%(2*np.pi)),vmin = 0, vmax = 2*np.pi, cmap = gray())
        imsave(f3, abs((phase(field_all) + 2*np.pi)%(2*np.pi)),vmin = 0, vmax = 2*np.pi, cmap = gray()) 
        rel_int[i] = (zod_new - zod_nosupp)/zod_nosupp * 100
        metho_field_fft_no_expand = ft.fftshift(ft.fft2(metho_field))
        rel_phase[i] = (phase(metho_field_fft_no_expand[96, 96]) - zod_phase)
        
    f_name = 'rel_int'+str(k)+'run.txt'
    f_name2 = 'rel_phase' + str(k) + 'run.txt'
    f_name = os.path.join(path, f_name)
    f_name2 = os.path.join(path, f_name2)
    np.savetxt(f_name, rel_int)
    np.savetxt(f_name2, rel_phase)
    print k
