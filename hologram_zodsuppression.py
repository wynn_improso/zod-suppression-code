import numpy as np
from cmath import phase
from numpy.fft import fft2,fftshift
from pylab import imread,imshow,show,imsave, random, gray
import scipy.fftpack as ft
def amp(y):
        a = np.abs(y)
        return a

def positions(size):
	q = range(-size/2,size/2)
	[x,y] = np.meshgrid(q,q)
	return [x,y]

def phase(x):
        p = np.angle(x) #there is a built-in function in numpy to compute for the angle
        return p

#generates the n-order Chebyshev polynomial of the second kind
def Un(n,x):
	U0 = 1;
	U1 = 2*x
	if n==0:
		return U0 #returns zeroth order Chebyshev
	elif n == 1:
		return U1 #returns first order Chebyshev
	else:
		for i in range(2,n+1):
			Un = 2*x*U1-U0 #Chebyshev recurrence relation
			U0 = U1 #updates polynomials
			U1 = Un
		return Un

def mslit(x,y,z,wavelength,fillfactor,aperture):
	#(x,y,z) are positions of the field point
	#aperture is the aperture size
	#Models the dead regions as multiple slit diffraction

	#initializes parameters
	nx = x.shape[0] #number of slits along x
	ny = y.shape[1] #number of slits along y
	dx = aperture/nx #distance between slits along x
	dy = aperture/ny #distance between slits along y
	ax = dx*np.sqrt(1-fillfactor) #width of slits along x
	ay = dy*np.sqrt(1-fillfactor) #width of slits along y 
	kx = 2*np.pi/wavelength*x/z #wave vector along x
	ky = 2*np.pi/wavelength*y/z #wave vector along y

	#calculates the field
	Ex = Un(nx-1,np.cos(kx * dx/2)) *  np.sin(kx * ax/2 +1e-34)/(nx * (kx * ax/2 +1e-34)) # field along x
	Ey = Un(ny-1,np.cos(ky * dy/2)) * np.sin(ky * ay/2 +1e-34)/(ny * (ky * ay/2 +1e-34)) # field along y
	return Ex*Ey

def AngularSpectrumWithLens(field,x,y,z,wavelength,focus):
	#displaces field from focus
	#initialize parameters
	k = 2*np.pi/wavelength
	qx = x/float(f) #"frequencies"
	qy = y/float(f)

	Uprime = fftshift(fft2(field)) #Angular spectrum of the field
	H = np.exp(1j*k*z*np.sqrt(1-qx**2-qy**2)) #Angular spectrum transfer function
	Uprime = Uprime*H
	U = fftshift(fft2(Uprime))
	return U

def AngularSpectrumTransferwithLens(x,y,z,wavelength,focus):
        k = 2*np.pi/wavelength
        qx = x/float(focus) #"frequencies"
        qy = y/float(focus)
        H = np.exp(1j*k*z*np.sqrt(1-qx**2-qy**2)) #Angular spectrum transfer function
        return H

def getphase(field):
	[sx,sy] = np.shape(field)
	#print [sx,sy]
	phi = np.zeros([sx,sy],dtype='float')
	#print phi
	for i in range(0,sx):
		for j in range(0,sx):
			#print phase(field[i,j])
			phi[i,j] = phase(field[i,j])
	return phi

def gerchberg_saxton(S,T):
        #note: look at the places where i used fftshift.  Remember your lens performs an optical FT which does NOT
        #require an fftshift while the function fft2 does.  When writing your code, imagine the physical setup.
        dim=np.shape(S)
        guess = random(dim)*2*np.pi
        a = ft.ifft2(amp(ft.fftshift(T))*np.exp(1j*random(dim)*2*np.pi))  #py.random is not really required. this just serves as a starting "guess" for the phase
        for m in range(41):
                b = amp(S)*np.exp(1j*phase(a))
                c = ft.fft2(b)
                d = amp(ft.fftshift(T))*np.exp(1j*phase(c))
                a = amp(S)*np.exp(1j*phase(ft.ifft2(d)))
                Rphase=phase(a)
        return Rphase
def circle(size, af):
    X = np.zeros([size,size])
    X[:,] = abs(np.linspace(0,size-1,size)-size/2)
    return (np.sqrt(X**2 + (X.T)**2) < af*size/2.)*1.

[x,y] = positions(768)
focus = 300 #300 mm focus lens
wavelength = 0.0006328 #632.8 nm wavelength

#Coordinates on the hologram plane
X = x*20.0/768 #Assumes 20mm window
Y = y*20.0/768 

#Coordinates on the reconstruction plane
Xprime = wavelength*focus*x/20
Yprime = wavelength*focus*y/20
Wprime = wavelength*focus*768/20

#source for image and image target
s = circle(768,1)
target = imread("target2.png")

#Solves for the ZOD field and target field
zod1 = mslit(Xprime,Yprime,focus,wavelength,0.8,Wprime) #ZOD
zod2 = s*ft.fftshift(ft.ifft2(zod1))
zod = (ft.fft2(zod2))
imsave('zod2.png', abs(zod), cmap = gray())
zod_field_phase = gerchberg_saxton(s, zod)
target_field_phase = gerchberg_saxton(s, target)
print 'gs finished'
imsave('target_hologram.png', target_field_phase, vmax = 2.55*np.pi, cmap = gray())
for i in range(0, 30):
        phase_shift = np.pi * i/10.
        total_field = s*np.exp(1j*(zod_field_phase + phase_shift)) + s*np.exp(1j*target_field_phase)
        holo = (getphase(total_field) + 2*np.pi)%(2*np.pi)
        imsave('./holograms1/holo_'+str(int(i+1))+'.png',holo,cmap='gray',vmin=0,vmax = 2.55*np.pi)
        

##ZOD = fft2(fftshift(zod))
##zodholo = getphase(ZOD)
##aperture = ((X**2+Y**2)<=10.0)*1.0
##uin = aperture*(np.exp(1j*zodholo)+AngularSpectrumTransferwithLens(X,Y,10,wavelength,focus))
##
###generates holograms for experiment with varying constant phase shift
##for i in range(0,100):
##	phaseshift = np.pi*i/50
##	uin = uin*np.exp(1j*phaseshift) #adds a constant phase shift for destructive interference
##	holo = getphase(uin)+np.pi
##	imsave('./holograms/holo_'+str(int(i+1))+'.png',holo,cmap='gray',vmin=0,vmax = 2*np.pi)
##
###I = abs(U)**2# + abs(Ey)**2
###imshow(zodholo)
###show()
